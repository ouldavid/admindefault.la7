<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="format-detection" content="telephone=no" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') | {{ $host_name }}</title>

    <meta name="robots" content="index, follow">
    <meta name="description" content="Poraman គេហទំព័រពត៌មានសម្រាប់មនុស្សជំនាន់ថ្មី" />
    <meta name="keywords" content="top news, hot news, entertainment news, radio drama, e-sport, chakdoat, social, abstract, beauty, press release, special talent, local food and TV program, cambodia, phnom penh" />
    <meta name="author" content="{{ $host_name }}" />

    <!-- google verify -->
    <meta name="google-site-verification" content="DRzWhb_w8f-86X-41jAzur50O1EoIqejvJatE82ae4M" />

    <!-- Schema.org markup for Google+ -->
    <meta itemprop="name" content="" />
    <meta itemprop="description" content="" />
    <meta itemprop="image" content="" />

    <!-- Twitter Card data -->
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:site" content="@PoramanC" />
    <meta name="twitter:creator" content="@PoramanC" />
    <meta name="twitter:domain" content="{{ Request::root() }}" />
    <meta name="twitter:url" content="{{ url()->current() }}" />
    <meta name="twitter:title" content="" />
    <meta name="twitter:description" content="" />
    <meta name="twitter:image" content="" />

    <!-- FB meta -->
    <meta property="fb:pages" content="379963682454063" />
    <meta property="fb:app_id" content="443382412765894" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="" />
    <meta property="og:title" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />
    <meta property="og:image:url" content="" />

    <!-- Canonical -->
    <link rel="canonical" href="{{ url()->current() }}" />

    <!-- app -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <!-- style -->
    <link href="{{ asset('css/common.css?v='.$version) }}" rel="stylesheet" type="text/css" />

    <!-- page style -->
    @yield('css')

    {{--<!-- Global Site Tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-115551422-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-115551422-1');
    </script>

    <!-- share social button -->
    <script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5a90e413992ac400137609bf&product=inline-share-buttons' async='async'></script>
--}}
</head>
<body>
        <header>
            @include('templates.header')
        </header>

        <!-- banner slide -->
        @yield('banner')

        <!-- content -->
        @yield('content')

        <footer>
            @include('templates.footer')
        </footer>

        <!-- Import JS -->
        <script src="{{ asset('js/app.js') }}" ></script>
        <script type="text/javascript" src="{{ asset('js/common.js?v='.$version) }}"></script>
        <!-- script page -->
        @yield('script')
</body>
</html>
